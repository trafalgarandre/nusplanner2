class Modship < ApplicationRecord
    belongs_to :mod
    belongs_to :pre_mod, class_name: "Mod"
end
