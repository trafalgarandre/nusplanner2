class Mod < ApplicationRecord
    has_many :modships
    has_many :pre_mods, through: "Modship"
    before_save :default_values
    def default_values
        self.taken ||= false
        self.pos ||= false
        self.mc ||= 4
        self.kind ||= "PR"
    end
end
