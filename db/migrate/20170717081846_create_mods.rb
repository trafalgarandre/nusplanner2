class CreateMods < ActiveRecord::Migration[5.0]
  def change
    create_table :mods do |t|
      t.string :code
      t.integer :mc
      t.string :kind
      t.boolean :taken
      t.boolean :pos

      t.timestamps
    end
  end
end
