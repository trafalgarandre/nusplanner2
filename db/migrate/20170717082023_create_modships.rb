class CreateModships < ActiveRecord::Migration[5.0]
  def change
    create_table :modships do |t|
      t.integer :mod_id
      t.integer :pre_mod_id

      t.timestamps
    end
  end
end
