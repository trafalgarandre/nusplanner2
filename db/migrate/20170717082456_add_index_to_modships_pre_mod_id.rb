class AddIndexToModshipsPreModId < ActiveRecord::Migration[5.0]
  def change
    add_index :modships, :pre_mod_id, unique:true
  end
end
