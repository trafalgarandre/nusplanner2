class AddLevelToMods < ActiveRecord::Migration[5.0]
  def change
    add_column :mods, :level, :string
  end
end
