# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# cs1010 = Mod.create({ code:"CS1010" }, ...)
# ....

# To make cs2020 have cs1010 has a prerequisite

#cs2020.modships.create(:pre_mod_id => cs1010.id)

